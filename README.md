## git-repo manifest

https://gerrit.googlesource.com/git-repo/

```sh
mkdir my-keid-fork; cd my-keid-fork

repo init -b main git@gitlab.com:keid/repo-manifest

repo sync

repo status
```

Next, install git LFS:

```fish
# fish
for path in (~/.bin/repo list -p)
    cd $path
    git lfs install
    git lfs fetch
    git lfs checkout
    cd -
end
```
